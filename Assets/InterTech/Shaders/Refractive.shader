﻿Shader "InterTech/Refractive" {
Properties {
	_Color ("Tint Color", Color) = (1,1,1,1)
	_MainTex ("Main Texture", 2D) = "white" {}
	[NoScaleOffset] _BumpMap ("Normal Map", 2D) = "bump" {}
    _IOR ("Index of Refraction",  Range(0,3)) = 1.5
    _Refract ("Refraction Amount", Range(0,1)) = 0.1
    _DistanceFalloff ("Camera Distance Falloff", Range(0,1)) = 0
    _Roughness ("Roughness", Range(0,1)) = 0
}
SubShader {
    // Draw ourselves after all opaque geometry
    Tags { "RenderType"="Transparent" "Queue"="Transparent" }

    // Grab the screen behind the object into _GrabTexture
    GrabPass { }
    
    CGPROGRAM
    #pragma surface surf Standard fullforwardshadows

    sampler2D _MainTex;
    sampler2D _BumpMap;
    sampler2D _GrabTexture;
    float4 _GrabTexture_TexelSize;
    float4 _Color;
    float _IOR;
    float _Refract;
    float _Roughness;
    float _DistanceFalloff;

    struct Input {
        float2 uv_MainTex;
        // float4 screenPos;
        float3 worldPos;
        float3 worldNormal;
        float3 viewDir;
        INTERNAL_DATA
    };

    void surf (Input IN, inout SurfaceOutputStandard o) {
        float4 grab = ComputeGrabScreenPos(UnityWorldToClipPos(IN.worldPos));
        float2 grabUV = grab.xy / grab.w;//IN.screenPos.xy / max(0.001, IN.screenPos.w);
        o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
        float camDist = distance(IN.worldPos, _WorldSpaceCameraPos);
        grabUV.xy += refract(IN.viewDir, WorldNormalVector(IN, o.Normal), _IOR) * max(_Refract - camDist * _DistanceFalloff, 0);
        float3 refract = tex2D( _GrabTexture, grabUV);
        float4 col = tex2D(_MainTex, IN.uv_MainTex) * _Color;

        o.Albedo = lerp(refract, col.rgb, col.a);
        o.Alpha = col.a;
        o.Smoothness = 1-_Roughness;
        o.Metallic = 0;
    }
    ENDCG
}
}