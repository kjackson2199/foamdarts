﻿Shader "InterTech/Vertex Colored Height Mix"
{
    Properties
    {
        _MainTex ("Primary Albedo (RGB)", 2D) = "white" {}
        _MainHeight ("Primary Height", 2D) = "black" {}
        _MainBump ("Primary Normal Map", 2D) = "bump" {}
        _AltTex ("Secondary Albedo (RGB)", 2D) = "white" {}
        _AltHeight ("Secondary Height", 2D) = "black" {}
        _AltBump ("Secondary Normal Map", 2D) = "bump" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Contrast ("Height Blend Contrast", Float) = 1
        _WaterLevel ("Water Line (World Space)", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _MainHeight;
        sampler2D _MainBump;
        sampler2D _AltTex;
        sampler2D _AltHeight;
        sampler2D _AltBump;

        struct Input
        {
            float4 color : COLOR;
            float2 uv_MainTex;
            float2 uv_AltTex;
            float worldHeight;
        };

        half _Contrast;
        half _WaterLevel;
        half _Glossiness;
        half _Metallic;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.worldHeight = v.vertex.z;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed3 c1 = tex2D(_MainTex, IN.uv_MainTex).rgb * IN.color.rgb;
            fixed3 c2 = tex2D(_AltTex, IN.uv_AltTex).rgb;
            fixed3 n1 = UnpackNormal(tex2D(_MainBump, IN.uv_MainTex));
            fixed3 n2 = UnpackNormal(tex2D(_AltBump, IN.uv_AltTex));
            float h1 = tex2D(_MainHeight, IN.uv_MainTex).r;
            float h2 = tex2D(_AltHeight, IN.uv_AltTex).r;


            float blend = saturate(h2 * h2 + IN.color.a * _Contrast - h1 * h1 * _Contrast);
            fixed3 c = lerp(c1, c2, blend);
            float h = lerp(h1, h2, blend);

            float wet = saturate(_WaterLevel - IN.worldHeight - h);
            c = lerp(c, c * 0.7f, wet);
            float s = lerp(_Glossiness, 0.9f, wet);


            o.Albedo = c;
            o.Normal = lerp(n1, n2, blend);

            o.Metallic = _Metallic;
            o.Smoothness = s;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
