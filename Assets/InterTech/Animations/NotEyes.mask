%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: NotEyes
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: beard
    m_Weight: 1
  - m_Path: body
    m_Weight: 1
  - m_Path: clothes
    m_Weight: 1
  - m_Path: eyes
    m_Weight: 0
  - m_Path: hair
    m_Weight: 1
  - m_Path: Nolan
    m_Weight: 1
  - m_Path: Nolan/Hips
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperLeft/LegLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperLeft/LegLeft/AnkleLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperLeft/LegLeft/AnkleLeft/FootLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperLeft/LegLeft/AnkleLeft/FootLeft/ToesLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperLeft/LegLeft/AnkleLeft/FootLeft/ToesLeft/ToesLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperRight
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperRight/LegRight
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperRight/LegRight/AnkleRight
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperRight/LegRight/AnkleRight/FootRight
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperRight/LegRight/AnkleRight/FootRight/ToesRight
    m_Weight: 1
  - m_Path: Nolan/Hips/LegUpperRight/LegRight/AnkleRight/FootRight/ToesRight/ToesRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/BrowInnerLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/BrowInnerLeft/BrowInnerLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/BrowInnerRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/BrowInnerRight/BrowInnerRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/BrowOuterLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/BrowOuterLeft/BrowOuterLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/BrowOuterRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/BrowOuterRight/BrowOuterRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/CheekLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/CheekLeft/CheekLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/CheekRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/CheekRight/CheekRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/Jaw
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/Jaw/Jaw_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LeftEye
    m_Weight: 0
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LeftEye/LeftEye_end
    m_Weight: 0
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LidLowerLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LidLowerLeft/LidLowerLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LidLowerRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LidLowerRight/LidLowerRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LidUpperLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LidUpperLeft/LidUpperLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LidUpperRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LidUpperRight/LidUpperRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LipLowerLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LipLowerLeft/LipLowerLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LipLowerRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LipLowerRight/LipLowerRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LipUpperLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LipUpperLeft/LipUpperLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LipUpperRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/LipUpperRight/LipUpperRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/MouthCornerLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/MouthCornerLeft/MouthCornerLeft_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/MouthCornerRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/MouthCornerRight/MouthCornerRight_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/RightEye
    m_Weight: 0
  - m_Path: Nolan/Hips/Spine/Chest/Neck/Head/RightEye/RightEye_end
    m_Weight: 0
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/IndexFinger0Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/IndexFinger0Left/IndexFinger1Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/IndexFinger0Left/IndexFinger1Left/IndexFinger2Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/IndexFinger0Left/IndexFinger1Left/IndexFinger2Left/IndexFinger2Left_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/MiddleFinger0Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/MiddleFinger0Left/MiddleFinger1Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/MiddleFinger0Left/MiddleFinger1Left/MiddleFinger2Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/MiddleFinger0Left/MiddleFinger1Left/MiddleFinger2Left/MiddleFinger2Left_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/PinkyFinger0Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/PinkyFinger0Left/PinkyFinger1Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/PinkyFinger0Left/PinkyFinger1Left/PinkyFinger2Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/PinkyFinger0Left/PinkyFinger1Left/PinkyFinger2Left/PinkyFinger2Left_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/RingFinger0Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/RingFinger0Left/RingFinger1Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/RingFinger0Left/RingFinger1Left/RingFinger2Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/RingFinger0Left/RingFinger1Left/RingFinger2Left/RingFinger2Left_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/Thumb0Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/Thumb0Left/Thumb1Left
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderLeft/ArmLeft/ForearmLeft/HandLeft/Thumb0Left/Thumb1Left/Thumb1Left_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/IndexFinger0Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/IndexFinger0Right/IndexFinger1Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/IndexFinger0Right/IndexFinger1Right/IndexFinger2Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/IndexFinger0Right/IndexFinger1Right/IndexFinger2Right/IndexFinger2Right_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/MiddleFinger0Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/MiddleFinger0Right/MiddleFinger1Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/MiddleFinger0Right/MiddleFinger1Right/MiddleFinger2Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/MiddleFinger0Right/MiddleFinger1Right/MiddleFinger2Right/MiddleFinger2Right_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/PinkyFinger0Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/PinkyFinger0Right/PinkyFinger1Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/PinkyFinger0Right/PinkyFinger1Right/PinkyFinger2Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/PinkyFinger0Right/PinkyFinger1Right/PinkyFinger2Right/PinkyFinger2Right_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/RingFinger0Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/RingFinger0Right/RingFinger1Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/RingFinger0Right/RingFinger1Right/RingFinger2Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/RingFinger0Right/RingFinger1Right/RingFinger2Right/RingFinger2Right_end
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/Thumb0RIght
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/Thumb0RIght/Thumb1Right
    m_Weight: 1
  - m_Path: Nolan/Hips/Spine/Chest/ShoulderRight/ArmRight/ForearmRight/HandRight/Thumb0RIght/Thumb1Right/Thumb1Right_end
    m_Weight: 1
  - m_Path: shoes
    m_Weight: 1
  - m_Path: teeth
    m_Weight: 1
