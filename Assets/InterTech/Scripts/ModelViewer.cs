﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelViewer : MonoBehaviour
{
    public static ModelViewer instance;

    [Range(1, 10)] public float lerpSpeed = 2;
    [Range(1, 360), Tooltip("deg/s")]public float turnSpeed = 15;
    public Transform yAxis;
    Vector3 targetRot;
    Vector3 rot;

    void Awake () {
        if (instance == null) instance = this;
    }

    void Start () {
        if (yAxis == null) yAxis = transform.GetChild(0);
    }

    void Update () {
        if (!Interactable.interacting) {
            rot = Vector3.zero;
            targetRot = Vector3.zero;
            return;
        }

        float yRot = Input.GetAxis("Mouse X") * turnSpeed * Time.unscaledDeltaTime;
        float xRot = -Input.GetAxis("Mouse Y") * turnSpeed * Time.unscaledDeltaTime;
        targetRot += new Vector3(xRot, yRot, 0);
        targetRot.x = Mathf.Clamp(targetRot.x, -80, 80);
        rot = Vector3.Lerp(rot, targetRot, Time.unscaledDeltaTime * lerpSpeed);
        yAxis.localEulerAngles = Vector3.up * rot.y;
        transform.localEulerAngles = Vector3.right * rot.x;
    }
}
