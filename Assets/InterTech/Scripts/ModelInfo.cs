﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Extensions;

public class ModelInfo : Interactable {
    
    static float duration = 0.5f;
    static float size = 0.05f;
    public string title;
    public string author;
    [TextArea(5,20)] public string description;
    public string animName;

    public override void Interact () {
        StartCoroutine(InteractionCoroutine());
    }

    WaitForEndOfFrame wait = new WaitForEndOfFrame();
    bool done;
    IEnumerator InteractionCoroutine () {
        _interacting = true;
        Time.timeScale = 0;
        
        UIController.ShowModelInfo(this);
        Vector3 startPos = transform.position;
        Quaternion startRot = transform.rotation;
        Vector3 startSize = transform.localScale;
        Bounds b = gameObject.RendererBounds();
        Vector3 ext = b.extents;
        float scale = (ext.x + ext.y + ext.z) / 3f;
        Vector3 endSize = Vector3.one * size / scale;
        Vector3 offset = -transform.InverseTransformPoint(b.center);
        Transform oldParent = transform.parent;
        transform.SetParent(ModelViewer.instance.yAxis);
        for (float t = 0; t <= duration; t += Time.unscaledDeltaTime) {
            float frac = t / duration;
            transform.position = Vector3.Lerp(startPos, ModelViewer.instance.yAxis.TransformPoint(offset), frac);
            transform.rotation = Quaternion.Slerp(startRot, ModelViewer.instance.yAxis.rotation, frac);
            ModelViewer.instance.yAxis.localScale = Vector3.Lerp(Vector3.one, endSize, frac);
            yield return wait;
        }
        transform.position = ModelViewer.instance.yAxis.TransformPoint(offset);
        transform.rotation = ModelViewer.instance.yAxis.rotation;
        ModelViewer.instance.yAxis.localScale = endSize;

        done = false;
        yield return new WaitUntil(() => done);
        transform.SetParent(oldParent);
        Vector3 endPos = transform.position;
        endSize = transform.localScale;
        for (float t = 0; t <= duration; t += Time.unscaledDeltaTime) {
            float frac = t / duration;
            transform.position = Vector3.Lerp(endPos, startPos, frac);
            transform.rotation = Quaternion.Slerp(ModelViewer.instance.yAxis.rotation, startRot, frac);
            transform.localScale = Vector3.Lerp(endSize, startSize, frac);
            yield return wait;
        }

        UIController.HideModelInfo();

        transform.position = startPos;
        transform.rotation = startRot;
        transform.localScale = startSize;
        ModelViewer.instance.yAxis.localScale = Vector3.one;
        _interacting = false;
        Time.timeScale = 1;
    }

    void Update () {
        if (Input.GetButtonDown("Cancel") || Input.GetMouseButtonDown(0)) done = true;
    }

    void OnDrawGizmos () {
        Bounds b = gameObject.RendererBounds();
        Gizmos.DrawLine(b.center - transform.right*0.01f, b.center + transform.right*0.01f);
        Gizmos.DrawLine(b.center - transform.up*0.01f, b.center + transform.up*0.01f);
        Gizmos.DrawLine(b.center - transform.forward*0.01f, b.center + transform.forward*0.01f);
    }
}
