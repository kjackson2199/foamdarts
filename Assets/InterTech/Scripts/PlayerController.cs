﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Range(1, 10)] public float lerpSpeed = 2;
    [Range(0.1f, 10), Tooltip("m/s")]public float moveSpeed = 3;
    [Range(1, 360), Tooltip("deg/s")]public float turnSpeed = 15;
    public float maxDegPerFrame = 10;
    public float jumpHeight = 1;
    public Transform look;
    CharacterController motor;
    Vector3 targetRot;
    Vector3 rot;
    Vector3 velocity;
    void Start () {
        Cursor.lockState = CursorLockMode.Locked;
        motor = GetComponent<CharacterController>();
        if (look == null) look = transform.GetChild(0);
        rot = new Vector3(look.localEulerAngles.x, transform.eulerAngles.y,0);
        targetRot = rot;
    }

    void Update () {
        if (Interactable.interacting) return;

        float right = Input.GetAxis("Horizontal");
        float forward = Input.GetAxis("Vertical");
        Vector3 move = transform.forward * forward + transform.right * right;
        if (move.magnitude > 1) move.Normalize();
        move *= moveSpeed;
        if (Input.GetButton("Crouch")) move *= 0.5f;
        else if (Input.GetButton("Run")) move *= 2;
        move.y = velocity.y;
        velocity = Vector3.Lerp(velocity, move, Time.deltaTime * lerpSpeed);
        
        float deg = Mathf.Min(turnSpeed * Time.deltaTime, maxDegPerFrame);
        float yRot = Input.GetAxis("Mouse X") * deg;
        float xRot = -Input.GetAxis("Mouse Y") * deg;
        targetRot += new Vector3(xRot, yRot, 0);
        targetRot.x = Mathf.Clamp(targetRot.x, -60, 60);
        rot = Vector3.Lerp(rot, targetRot, Time.deltaTime * lerpSpeed);
        transform.eulerAngles = Vector3.up * rot.y;
        look.localEulerAngles = Vector3.right * rot.x;

        Vector3 lookRot = look.localEulerAngles;
        if (lookRot.x > 180) lookRot.x -= 360;
        if (lookRot.x < -180) lookRot.x += 360;
        lookRot.x = Mathf.Clamp(lookRot.x, -60, 60);
        look.localEulerAngles = lookRot;

        if (motor.isGrounded && Input.GetButtonDown("Jump")) {
            velocity.y = Mathf.Sqrt(2 * jumpHeight * Physics.gravity.magnitude);
        }

        if (motor.isGrounded && Input.GetButton("Crouch")) {
            look.localPosition = Vector3.Lerp(look.localPosition, Vector3.up, Time.deltaTime * lerpSpeed);
        }
        else {
            look.localPosition = Vector3.Lerp(look.localPosition, Vector3.up * 1.5f, Time.deltaTime * lerpSpeed);
        }

        if (!motor.isGrounded) velocity += Physics.gravity * Time.deltaTime;
        motor.Move(velocity * Time.deltaTime);
    }
}
