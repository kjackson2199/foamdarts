﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    protected static bool _interacting;
    public static bool interacting {
        get { return _interacting; }
    }

    public virtual void Interact () {

    }
}
