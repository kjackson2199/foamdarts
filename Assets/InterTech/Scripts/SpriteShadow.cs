﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteShadow : MonoBehaviour
{
    [ContextMenu("Setup")]
    void Setup () {
        Renderer r = GetComponent<Renderer>();
        r.receiveShadows = true;
        r.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
    }
}
