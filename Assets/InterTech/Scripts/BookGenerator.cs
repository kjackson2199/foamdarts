﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class BookGenerator : MonoBehaviour
{
    public int seed = 0;
    public Gradient gradient;
    [Range(0, 360)] public float maxRotation = 0;
    public Vector3 minSize = new Vector3(0.01f, 0.15f, 0.1f);
    public Vector3 maxSize = new Vector3(0.05f, 0.5f, 0.3f);
    [Range(0.05f, 2)] public float stackSize = 1;
    public bool drawEdge = true;
    public bool drawBottom = true;
    [Range(0, 1)] public float hardbackPercent = 0.1f;
    
    Mesh mesh;

    [ContextMenu("Create Mesh")]
    public void CreateMesh () {
        if (mesh == null) mesh = new Mesh();

        List<Vector3> positions = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();
        List<Color> colors = new List<Color>();
        List<int> triangles = new List<int>();

        int index = 0;
        float x = 0;
        Random.InitState(seed);
        while (x < stackSize) {
            Vector3 noisyDimensions = new Vector3(
                Random.Range(minSize.x, maxSize.x),
                Random.Range(minSize.y, maxSize.y),
                Random.Range(minSize.z, maxSize.z)
            );

            Vector3 lerpedDimensions = Vector3.Lerp(minSize, maxSize, Random.value);

            Vector3 s = Vector3.Lerp(noisyDimensions, lerpedDimensions, 0.7f);
            if (s.x == 0) s.x = 0.001f;
            else if (s.x < 0) s.x *= -1;
            
            if (x+s.x > stackSize) break;

            s.y *= 0.5f;
            s.z *= 0.5f;
            Vector3 offset = s;
            offset.x = x;
            offset.z *= -1;
            x += s.x;

            Quaternion rotation = Quaternion.AngleAxis(Random.value * maxRotation, Vector3.right);

            positions.AddRange(new Vector3[] {
                //spine
                rotation * new Vector3(  0, -s.y, -s.z) + offset,
                rotation * new Vector3(  0,  s.y, -s.z) + offset,
                rotation * new Vector3(s.x,  s.y, -s.z) + offset,
                rotation * new Vector3(s.x, -s.y, -s.z) + offset,

                //front
                rotation * new Vector3(s.x, -s.y, -s.z) + offset,
                rotation * new Vector3(s.x,  s.y, -s.z) + offset,
                rotation * new Vector3(s.x,  s.y,  s.z) + offset,
                rotation * new Vector3(s.x, -s.y,  s.z) + offset,

                //back
                rotation * new Vector3(  0, -s.y,  s.z) + offset,
                rotation * new Vector3(  0,  s.y,  s.z) + offset,
                rotation * new Vector3(  0,  s.y, -s.z) + offset,
                rotation * new Vector3(  0, -s.y, -s.z) + offset,

                //top
                rotation * new Vector3(s.x,  s.y, -s.z) + offset,
                rotation * new Vector3(  0,  s.y, -s.z) + offset,
                rotation * new Vector3(  0,  s.y,  s.z) + offset,
                rotation * new Vector3(s.x,  s.y,  s.z) + offset
            });

            if (drawBottom) positions.AddRange(new Vector3[] {
                //bottom
                rotation * new Vector3(  0, -s.y, -s.z) + offset,
                rotation * new Vector3(s.x, -s.y, -s.z) + offset,
                rotation * new Vector3(s.x, -s.y,  s.z) + offset,
                rotation * new Vector3(  0, -s.y,  s.z) + offset
            });

            if (drawEdge) positions.AddRange(new Vector3[] {
                //edge
                rotation * new Vector3(s.x, -s.y,  s.z) + offset,
                rotation * new Vector3(s.x,  s.y,  s.z) + offset,
                rotation * new Vector3(  0,  s.y,  s.z) + offset,
                rotation * new Vector3(  0, -s.y,  s.z) + offset
            });

            normals.AddRange(new Vector3[] {
                //spine
                Vector3.back, Vector3.back, Vector3.back, Vector3.back, 
                //front
                Vector3.right, Vector3.right, Vector3.right, Vector3.right, 
                //back
                Vector3.left, Vector3.left, Vector3.left, Vector3.left, 
                //top
                Vector3.up, Vector3.up, Vector3.up, Vector3.up
            });

            if (drawBottom) normals.AddRange(new Vector3[] {
                //bottom
                Vector3.down, Vector3.down, Vector3.down, Vector3.down
            });

            if (drawEdge) normals.AddRange(new Vector3[] {
                //edge
                Vector3.forward, Vector3.forward, Vector3.forward, Vector3.forward
            });

            bool isHardback = Random.value < hardbackPercent;
            float edgeYOffset = isHardback ? 0 : 0.5f;
            float coverXOffset = 0.4375f * Random.Range(0, 2);
            float coverYOffset = 0.25f * Random.Range(0, 4);
            uvs.AddRange(new Vector2[] {
                //spine
                new Vector2(coverXOffset + 0.1875f, coverYOffset + 0),
                new Vector2(coverXOffset + 0.1875f, coverYOffset + 0.25f),
                new Vector2(coverXOffset + 0.25f,   coverYOffset + 0.25f),
                new Vector2(coverXOffset + 0.25f,   coverYOffset + 0),
                //front
                new Vector2(coverXOffset + 0.25f,   coverYOffset + 0),
                new Vector2(coverXOffset + 0.25f,   coverYOffset + 0.25f),
                new Vector2(coverXOffset + 0.4375f, coverYOffset + 0.25f),
                new Vector2(coverXOffset + 0.4375f, coverYOffset + 0),
                //back
                new Vector2(coverXOffset + 0,       coverYOffset + 0),
                new Vector2(coverXOffset + 0,       coverYOffset + 0.25f),
                new Vector2(coverXOffset + 0.1875f, coverYOffset + 0.25f),
                new Vector2(coverXOffset + 0.1875f, coverYOffset + 0),
                //top
                new Vector2(0.875f, 1-edgeYOffset),
                new Vector2(1,      1-edgeYOffset),
                new Vector2(1,      0.55f-edgeYOffset),
                new Vector2(0.875f, 0.55f-edgeYOffset)
            });

            if (drawBottom) uvs.AddRange(new Vector2[] {
                //bottom
                new Vector2(0.875f, 1-edgeYOffset),
                new Vector2(1,      1-edgeYOffset),
                new Vector2(1,      0.55f-edgeYOffset),
                new Vector2(0.875f, 0.55f-edgeYOffset)
            });

            if (drawEdge) uvs.AddRange(new Vector2[] {
                //edge
                new Vector2(0.875f, 0.55f-edgeYOffset),
                new Vector2(0.875f, 0.952f-edgeYOffset),
                new Vector2(1,      0.952f-edgeYOffset),
                new Vector2(1,      0.55f-edgeYOffset)
            });

            Color color = gradient.Evaluate(Random.value);
            color.a = isHardback ? 0.1f : 0.8f;//smoothness
            colors.AddRange(new Color[] {
                color, color, color, color,
                color, color, color, color,
                color, color, color, color,
                color, color, color, color
            });
            if (drawBottom) colors.AddRange(new Color[] {color, color, color, color});
            if (drawEdge) colors.AddRange(new Color[] {color, color, color, color});

            triangles.AddRange(new int[] {
                index+0,  index+1,  index+2,  index+0,  index+2,  index+ 3,
                index+4,  index+5,  index+6,  index+4,  index+6,  index+ 7,
                index+8,  index+9,  index+10, index+8,  index+10, index+11,
                index+12, index+13, index+14, index+12, index+14, index+15
            });

            if (drawBottom || drawEdge) triangles.AddRange(new int[] {
                index+16, index+17, index+18, index+16, index+18, index+19
            });

            if (drawBottom && drawEdge) triangles.AddRange(new int[] {
                index+20, index+21, index+22, index+20, index+22, index+23,
            });

            index += 16;
            if (drawBottom || drawEdge) index += 4;
            if (drawBottom && drawEdge) index += 4;
        }

        mesh.Clear();
        mesh.vertices = positions.ToArray();
        mesh.normals = normals.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.colors = colors.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateBounds();

        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    // void OnDrawGizmos () {
    //     Gizmos.matrix = transform.localToWorldMatrix;

    //     Vector3 size = minSize;
    //     Vector3 center = size/2;
    //     center.z *= -1;
    //     Gizmos.color = Color.white;
    //     Gizmos.DrawWireCube(center, size);

    //     size = maxSize;
    //     center = size/2;
    //     center.z *= -1;
    //     Gizmos.color = Color.blue;
    //     Gizmos.DrawWireCube(center, size);

    //     size.x = stackSize;
    //     center = size/2;
    //     center.z *= -1;
    //     Gizmos.color = new Color(1,1,1,0.5f);
    //     Gizmos.DrawWireCube(center, size);
    // }
}
