﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Utils;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class RoadGenerator : MonoBehaviour
{
    [Range(0.5f, 20)] public float width = 1;
    public int segments = 4;
    public List<Vector3> path = new List<Vector3>(new Vector3[] { Vector3.zero, Vector3.forward });

    Mesh mesh;

    [ContextMenu("Create Mesh")]
    public void CreateMesh () {
        if (path.Count <= 1) return;

        if (mesh == null) mesh = new Mesh();
        else mesh.Clear();

        List<Vector3> positions = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<int> triangles = new List<int>();

        int index = 0;
        Vector3 start = path[0];
        Vector3 end = path[1];
        Vector3 prev = start*2 - end;
        Vector3 next = (path.Count<=2) ? (end*2-start) : path[2];
        Vector3 prevForward = (start - prev).normalized;
        float halfW = width * 0.5f;
        for (int i = 0; i < path.Count-1; i++) {
            float rad = Vector3.Angle(end - start, next - end) * Mathf.Deg2Rad;
            float h = halfW / Mathf.Sin(rad);//hypotnuse
            float d = halfW / Mathf.Tan(rad);//distance from end
            Vector3 diff = end - start;
            float dist = diff.magnitude;
            Vector3 dir = diff/dist;
            Vector3 tempEnd = end;
            // end = start + dir * (dist-d);
            for (int j = 0; j < segments; j++) {
                Vector3 a = Interpolate.CatmullRom(prev, start, end, next, (float)j / (float)segments);
                Vector3 b = Interpolate.CatmullRom(prev, start, end, next, (float)(j+1) / (float)segments);

                Vector3 forward = (b-a).normalized;
                Vector3 rightA = Vector3.Cross(Vector3.up, prevForward).normalized;
                Vector3 rightB = Vector3.Cross(Vector3.up, forward).normalized;
                prevForward = forward;
                positions.Add(a - rightA * halfW);
                positions.Add(b - rightB * halfW);
                positions.Add(b + rightB * halfW);
                positions.Add(a + rightA * halfW);

                uvs.Add(Vector2.zero);
                uvs.Add(Vector2.up);
                uvs.Add(Vector2.one);
                uvs.Add(Vector2.right);

                triangles.AddRange(new int[] {
                    index, index+1, index+2,
                    index, index+2, index+3
                });

                index += 4;
            }
            dir = (next - end).normalized;
            prev = start;
            start = end;//tempEnd + dir * d;
            end = next;
            next = (i+3>=path.Count) ? (end*2-start) : path[i+3];
        }

        mesh.vertices = positions.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        GetComponent<MeshFilter>().sharedMesh = mesh;
    }
}
