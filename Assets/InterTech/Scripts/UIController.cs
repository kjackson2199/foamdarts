﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    [Header("Reticle")]
    public Image reticle;
    public Sprite defaultReticleIcon;
    public Sprite inspectReticleIcon;
    public Sprite doorReticleIcon;

    [Header("Mini Map")]
    public Transform miniMapRoot;

    [Header("Model Info")]
    public Transform modelInfoRoot;
    public Text modelTitle;
    public Text modelAuthor;
    public Text modelDescription;

    void Awake () {
        if (instance == null) instance = this;
        HideModelInfo();
    }

    public static void UpdateReticle (Interactable interactable) {
        if (interactable == null) instance.reticle.sprite = instance.defaultReticleIcon;
        else if (interactable is ModelInfo) instance.reticle.sprite = instance.inspectReticleIcon;
        else if (interactable is DoorController) instance.reticle.sprite = instance.doorReticleIcon;
        else instance.reticle.sprite = instance.defaultReticleIcon;
    }

    public static void ShowModelInfo (ModelInfo info) {
        instance.reticle.gameObject.SetActive(false);
        instance.miniMapRoot.gameObject.SetActive(true);
        instance.modelInfoRoot.gameObject.SetActive(true);
        instance.modelTitle.text = info.title;
        instance.modelAuthor.text = "by " + info.author + "\n";
        instance.modelDescription.text = info.description;
    }

    public static void HideModelInfo () {
        instance.modelInfoRoot.gameObject.SetActive(false);
        instance.miniMapRoot.gameObject.SetActive(true);
        instance.reticle.gameObject.SetActive(true);
    }
}
