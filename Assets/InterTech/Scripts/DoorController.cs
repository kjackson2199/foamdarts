﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : Interactable {

    public float animationDuration = 1f;
    public float autoLockDelay = 5f;
    public float autoCloseDelay = 5f;
    public bool isOpen;
    public bool isLocked;
    public bool autoClose;
    public bool autoLock;
    public Vector3 openRotation;
    public Vector3 closedRotation;

    [ContextMenu("Set Open Rotation From Scene")]
    public void SetOpenRotationFromScene() {
        openRotation = transform.localEulerAngles;
    }

    [ContextMenu("Set Closed Rotation From Scene")]
    public void SetClosedRotationFromScene() {
        closedRotation = transform.localEulerAngles;
    }

    [ContextMenu("Open Door")]
    public void SetDoorOpen() {
        isOpen = true;
        transform.localEulerAngles = openRotation;
    }

    [ContextMenu("Close Door")]
    public void SetDoorClosed() {
        isOpen = false;
        transform.localEulerAngles = closedRotation;
    }

    public override void Interact () {
        StopAllCoroutines();
        StartCoroutine(InteractionCoroutine());
    }

    public void UnlockDoor () {
        isLocked = false;
        if (autoLock) StartCoroutine(AutoLockCoroutine());
    }

    IEnumerator AutoLockCoroutine () {
        yield return new WaitForSeconds(autoLockDelay);    
    }

    IEnumerator InteractionCoroutine () {
        if (!isOpen && isLocked) {
            Debug.Log("DOOR IS LOCKED, DISPLAY MESSAGE TO USER!!!!");
            yield break;
        }

        yield return StartCoroutine(OpenCloseCoroutine());

        if (isOpen && autoClose) {
            yield return new WaitForSeconds(autoCloseDelay);    
            yield return StartCoroutine(OpenCloseCoroutine());
        }

        if (!isOpen && autoLock) isLocked = true;
    }

    WaitForEndOfFrame wait = new WaitForEndOfFrame();
    IEnumerator OpenCloseCoroutine () {
        Quaternion start = transform.localRotation;
        Quaternion end = Quaternion.Euler( isOpen ? closedRotation : openRotation );
        isOpen = !isOpen;
        for (float t = 0; t < animationDuration; t += Time.deltaTime) {
            float frac = t / animationDuration;
            transform.localRotation = Quaternion.Slerp(start, end, frac);
            yield return wait;
        }
    }
}
