﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RoadGenerator))]
public class RoadGeneratorEditor : Editor
{
    public RoadGenerator road {
        get { return target as RoadGenerator; }
    }

    void OnEnable()
    {
        Undo.undoRedoPerformed += UndoRedo;
    }

    void OnDisable()
    {
        Undo.undoRedoPerformed -= UndoRedo;
    }

    void UndoRedo ()
    {
        road.CreateMesh();
    }

    void OnSceneGUI()
    {
        SceneView view = SceneView.lastActiveSceneView;        
        Event e = Event.current;

        bool adding = e.modifiers == EventModifiers.Shift;

        if (e.modifiers == EventModifiers.Control) Handles.color = Color.red;
        else Handles.color = Color.white;

        if (e.type == EventType.MouseDown)
        {
            if (e.modifiers == EventModifiers.Control) RemovePoint();
            else if (e.modifiers == EventModifiers.Shift) AddPoint(); 
        }

        EditorGUI.BeginChangeCheck();
        Quaternion rot = view.camera.transform.rotation;
        Handles.matrix = road.transform.localToWorldMatrix;
        Undo.RecordObject(road, "Changed path of road.");
        for (int i = 0; i < road.path.Count; i++)
        {
            road.path[i] = Handles.FreeMoveHandle(road.path[i], rot, 1, Vector3.zero, Handles.CircleHandleCap);
            if (i != 0) {
                Handles.DrawLine(road.path[i-1], road.path[i]);
                if (adding) {
                    Handles.color = Color.green;
                    Vector3 center = road.path[i-1] + road.path[i];
                    Handles.DrawWireDisc(center*0.5f, -view.camera.transform.forward, 0.5f);
                    Handles.color = Color.white;
                }
            }
        }
        if (EditorGUI.EndChangeCheck())
        {
            road.CreateMesh();
        }
    }

    void AddPoint ()
    {
        SceneView view = SceneView.lastActiveSceneView;
        Camera cam = view.camera;
        Vector2 mouse = Event.current.mousePosition;
        mouse.y = view.position.height * 2 - Screen.height - Event.current.mousePosition.y;
        for (int i = 1; i < road.path.Count; i++)
        {
            Vector3 midpoint = (road.path[i-1] + road.path[i]) * 0.5f;
            Vector3 worldPos = road.transform.TransformPoint(midpoint);
            Vector2 screenPos = cam.WorldToScreenPoint(worldPos);
            float distSq = (screenPos - mouse).sqrMagnitude;
            float size = HandleUtility.GetHandleSize(worldPos) * 0.5f;
            if (distSq < size*size)
            {
                Event.current.Use();
                Undo.RecordObject(road, "Removed path point from road.");
                road.path.Insert(i, midpoint);
                road.CreateMesh();
                return;
            }
        }
    }

    void RemovePoint ()
    {
        SceneView view = SceneView.lastActiveSceneView;
        Camera cam = view.camera;
        Vector2 mouse = Event.current.mousePosition;
        mouse.y = view.position.height * 2 - Screen.height - Event.current.mousePosition.y;
        for (int i = 0; i < road.path.Count; i++)
        {
            Vector3 worldPos = road.transform.TransformPoint(road.path[i]);
            Vector2 screenPos = cam.WorldToScreenPoint(worldPos);
            float distSq = (screenPos - mouse).sqrMagnitude;
            float size = HandleUtility.GetHandleSize(worldPos);
            if (distSq < size*size)
            {
                Event.current.Use();
                Undo.RecordObject(road, "Removed path point from road.");
                road.path.RemoveAt(i);
                road.CreateMesh();
                return;
            }
        }
    }
}
